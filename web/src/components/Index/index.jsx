// @flow
/*  eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import './style.css';

type IndexProps = {
  profile: object
}

const Index = ({ profile }: IndexProps) => {
  if (profile == null) {
    console.log('redirect to signin');
    return <Redirect push to="/signin" />;
  }

  const { name, imageUrl } = profile;
  return (
    <div className="profile">
      <img src={imageUrl} alt="user profile" />
      <h2>hello { name }</h2>
      <div>
        <Link to="/signout">Sign out</Link>
      </div>
    </div>
  );
};

export default Index;
