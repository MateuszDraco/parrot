// @flow
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import SignIn from './SignIn';
import SignOut from './SignOut';
import Index from './Index';

type DispatcherProps = {
  profile: object,
  profileUpdate: (object) => void
}

class Dispatcher extends React.Component<DispatcherProps> {
  constructor(props) {
    super(props);

    this.onSignIn = this.onSignIn.bind(this);
    this.onSignOut = this.onSignOut.bind(this);
  }

  onSignIn(profile) {
    const { profileUpdate } = this.props;
    profileUpdate(profile);
  }

  onSignOut() {
    const { profileUpdate } = this.props;
    profileUpdate(null);
  }

  render() {
    const { profile } = this.props;
    return (
      <Router>
        <Switch>
          <Route path="/" exact render={props => <Index {...props} profile={profile} />} />
          <Route path="/signin" render={props => <SignIn {...props} onSignIn={this.onSignIn} profile={profile} />} />
          <Route path="/signout" render={props => <SignOut {...props} onSignOut={this.onSignOut} />} />
        </Switch>
      </Router>
    );
  }
}

export default Dispatcher;
