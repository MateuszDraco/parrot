// @flow
import React from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import { API_HOST } from '../config';

type Props = {
  onSignIn: (object) => void,
  profile: object,
}

class SignIn extends React.Component<Props> {
  constructor(props) {
    super(props);

    this.handleSignIn = this.handleSignIn.bind(this);
    this.onGApiLoaded = this.onGApiLoaded.bind(this);
  }

  componentDidMount() {
    console.log('signIn did mount');
    if (!this.isAlreadySignedIn()) {
      this.onGApiLoaded();
    }
  }

  onGApiLoaded() {
    window.gapi.signin2.render('google-signin', {
      scope: 'profile email',
      width: 240,
      height: 50,
      longtitle: true,
      theme: 'dark',
      onsuccess: this.handleSignIn,
      onfailure: SignIn.onFailure,
    });
  }

  isAlreadySignedIn() {
    const { profile } = this.props;
    return profile != null;
  }

  handleSignIn(user) {
    const { onSignIn } = this.props;
    const googleProfile = user.getBasicProfile();
    const authResponse = user.getAuthResponse();
    const profile = {
      name: googleProfile.getGivenName(),
      imageUrl: googleProfile.getImageUrl(),
      token: authResponse.id_token,
    };
    axios.post(`https://${API_HOST}/signin`, profile)
      .then((response) => {
        profile.authorization = response.headers['x-amzn-remapped-authorization'];
        onSignIn(profile);
      });
  }

  render() {
    if (this.isAlreadySignedIn()) {
      return <Redirect push to="/" />;
    }
    return <div id="google-signin" />;
  }
}

export default SignIn;
