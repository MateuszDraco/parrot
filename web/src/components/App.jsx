// @flow
import React from 'react';
import Dispatcher from './Dispatcher';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = { profile: null };
    this.profileUpdate = this.profileUpdate.bind(this);
  }

  profileUpdate(profile) {
    console.log(`profile update: ${JSON.stringify(profile)}`);
    this.setState({ profile });
  }

  render() {
    const { profile } = this.state;
    return <Dispatcher profile={profile} profileUpdate={this.profileUpdate} />;
  }
}

export default App;
