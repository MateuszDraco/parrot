// @flow
import React from 'react';
import { Link } from 'react-router-dom';

type Props = {
  onSignOut: () => void,
}

class SignOut extends React.Component<Props> {
  constructor(props) {
    super(props);

    this.handleSignOut = this.handleSignOut.bind(this);
  }

  componentDidMount() {
    const auth2 = window.gapi.auth2.getAuthInstance();
    auth2.signOut().then(this.handleSignOut);
  }

  handleSignOut() {
    const { onSignOut } = this.props;
    onSignOut();
  }

  render() {
    return (
      <div>
        <h2>Bye...</h2>
        <Link to="/signin">Sign in again</Link>
      </div>
    );
  }
}

export default SignOut;
