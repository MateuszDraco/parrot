'use strict';

const jwt = require('jsonwebtoken');

// Help function to generate an IAM policy
const generatePolicy = function(principalId, effect, resource) {
    var authResponse = {};

    authResponse.principalId = principalId;
    if (effect && resource) {
        var policyDocument = {};
        policyDocument.Version = '2012-10-17';
        policyDocument.Statement = [];
        var statementOne = {};
        statementOne.Action = 'execute-api:Invoke';
        statementOne.Effect = effect;
        statementOne.Resource = resource;
        policyDocument.Statement[0] = statementOne;
        authResponse.policyDocument = policyDocument;
    }

    // Optional output with custom properties of the String, Number or Boolean type.
    authResponse.context = {
        "stringKey": "stringval",
        "numberKey": 123,
        "booleanKey": true
    };
    return authResponse;
}

const secret = 'my.precious';

function decodeToken(token) {
  console.log('incoming token: ' + token);
  try {
    return jwt.verify(token, secret);
  } catch(err) {
    console.log('failed.');
    return null;
  }
}

module.exports.verify = async (event, context) => {
  const token = decodeToken(event.authorizationToken);
  console.log('decoded: ' + JSON.stringify(token));
  const policy = generatePolicy('user', token != null ? 'Allow' : 'Deny', event.methodArn);
  return policy;
}

module.exports.signin = async (event, context) => {
  const token = jwt.sign({ name: 'John Doe' }, secret);
  console.log('token: ' + token);

  const response = {
    statusCode: 200,
    headers: {
      Authorization: token,
     'Access-Control-Allow-Origin': "*",
     'Access-Control-Allow-Credentials': true,
     'Access-Control-Expose-Headers': 'x-amzn-remapped-Authorization',
    }
  };
  return response;
}
