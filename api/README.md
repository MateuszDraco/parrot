# Setup
To configure a **serverless** please head to [QuickStart](https://serverless.com/framework/docs/providers/aws/guide/quick-start/).

# Deployment
## Development
To deploy to AWS please use a command:
```
serverless deploy -v
```
## Service removal
To remove a service from AWS please use a command:
```
serverless remove
```
